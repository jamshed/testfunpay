<?php
function getDataFromSms(string $sms) {
	$result = [];
	if (preg_match('/(^|\s)([0-9]{4,6})($|\s|,[^0-9]|\.)/', $sms, $matches)) {
		$result['password'] = $matches[2];
	}

	if (preg_match('/(^|\s)([0-9]{1,}(|,[0-9]{1,2}))р/', $sms, $matches)) {
		$result['paySum'] = $matches[2];
	}

	if (preg_match('/(^|\s)([0-9]{15})($|\s|,[^0-9]|\.)/', $sms, $matches)) {
		$result['accountNumber'] = $matches[2];
	}

	if (count($result) == 3) {
		return $result;
	}

	return false;
}

$sms = 'Пароль: 1588 
Спишется 1500,48р.
Перевод на счет 410011640748554';

var_dump(getDataFromSms($sms));